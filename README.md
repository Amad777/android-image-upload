# Android Image Upload

Please go through this readme.

Most of the Android versions above 8 need you to add this to your application tag 
android:usesCleartextTraffic="true"
Some of them might also need
android:requestLegacyExternalStorage="true"
Add them or remove them based on your needs

#Library
We are using Android networking library for image upload, it makes code clean, no other magic.
Here is their github: https://github.com/amitshekhariitbhu/Fast-Android-Networking